package main

import (
	"flag"
	"fmt"
	"os"

	"copy2bagent/handlers"
)

func main() {

	c2bWrapper := handlers.C2BWrapper{}

	flag.StringVar(&c2bWrapper.IP, "i", "", "IP address of bagent")
	flag.IntVar(&c2bWrapper.Port, "p", -1, "port of bagent")
	flag.StringVar(&c2bWrapper.ProxyIP, "x", "", "IP addr of proxy")
	flag.IntVar(&c2bWrapper.ProxyPort, "P", -1, "port of proxy")
	flag.StringVar(&c2bWrapper.LocalFileAddr, "l", "", "local file")
	flag.StringVar(&c2bWrapper.DstDir, "d", "", "directory on bagent")
	flag.IntVar(&c2bWrapper.ThreadNum, "t", 1, "thread num")
	flag.Parse()

	if len(c2bWrapper.IP) == 0 && len(c2bWrapper.IPFileAddr) == 0 {
		fmt.Sprintf("IP or an IP file should be given")
		os.Exit(1)
	}
	if len(c2bWrapper.IP) != 0 && c2bWrapper.Port == -1 {
		fmt.Sprintf("the port should be given")
		os.Exit(1)
	}

	if len(c2bWrapper.ProxyIP) != 0 && c2bWrapper.ProxyPort == -1 {
		fmt.Sprintf("the port of proxy should be given")
		os.Exit(1)
	}

	if len(c2bWrapper.LocalFileAddr) == 0 {
		fmt.Sprintf("no file, exit")
		os.Exit(1)
	}

	if len(c2bWrapper.DstDir) == 0 {
		fmt.Sprintf("no dst dir, exit")
		os.Exit(1)
	}

	if c2bWrapper.ThreadNum < 0 {
		fmt.Sprintf("thread num must > 0")
		os.Exit(1)
	}

	c2bWrapper.Execute()

	os.Exit(0)
}
