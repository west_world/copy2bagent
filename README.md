## commit note 2018.06.01

copy2bagent 客户端功能如下

- 直接指定 bagent 的 IP，执行命令
e.g. ./copy2bagent -i 127.0.0.1 -p 8090 -l ./Makefile -d /tmp/
- 指定 IP 列表文件，执行命令 e.g. ./copy2bagent -f test.ip -l ./Makefile -d /tmp/
- 多线程执行，线程之间的内容不会交叉
- 只需要制定 IP 和端口号，"http://" 和 url 路径程序会自动填充
- 输出内容包括：来源 url，IO 错误，bagent 错误
- 封装的 post 命令：
e.g. curl -F "fileName=test" -F "uploadFile=@/tmp2/test" -F "dir=/tmp/"  127.0.0.1:8090/files
