package handlers

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

type C2BWrapper struct {
	ThreadNum     int
	IPFileAddr    string
	IP            string
	Port          int
	ProxyIP       string
	ProxyPort     int
	LocalFileAddr string
	DstDir        string
}

// 若 ProxyUrl 为空则直接向 TargetUrl 发起请求
type UrlWrapper struct {
	TargetUrl string
	ProxyUrl  string
}

/*
会有多个 Url 并发请求
*/
func (w *C2BWrapper) ParseUrls() (urls []*UrlWrapper, err error) {
	urls = []*UrlWrapper{}
	httpPre := "http://"

	if len(w.IPFileAddr) == 0 {
		urlWrapper := UrlWrapper{}
		urlWrapper.TargetUrl = fmt.Sprintf("%v%v:%v/files", httpPre, w.IP, w.Port)
		if len(w.ProxyIP) != 0 {
			urlWrapper.ProxyUrl = fmt.Sprintf("%v%v:%v", httpPre, w.ProxyIP, w.ProxyPort)
		}
		urls = append(urls, &urlWrapper)
		return
	}
	f, err := os.Open(w.IPFileAddr)
	if err != nil {
		return nil, err
	}
	buf := bufio.NewReader(f)
	for {
		lineBytes, _, err := buf.ReadLine()
		line := string(lineBytes)
		if err != nil && err != io.EOF {
			return urls, err
		}
		if io.EOF == err {
			break
		}

		line = strings.TrimSpace(line)

		if strings.HasPrefix(line, "#") {
			continue
		}
		if len(line) == 0 {
			continue
		}
		splits := strings.Split(line, "||")
		if len(splits) == 1 || len(splits) > 4 {
			return nil, errors.New("IP file format error")
		}
		urlWrapper := UrlWrapper{}
		urlWrapper.TargetUrl = fmt.Sprintf("%v%v:%v/bcmd", httpPre, splits[0], splits[1])

		if len(splits) == 4 {
			urlWrapper.ProxyUrl = fmt.Sprintf("%v%v:%v", httpPre, splits[2], splits[3])
		}
		urls = append(urls, &urlWrapper)
	}
	return urls, nil
}

type C2BResp struct {
	Code int64  `json:"code"`
	Msg  string `json:"msg"`
	Err  string `json:"err,omitempty"`
}

type B2CRespWrapper struct {
	SrcUrl string
	IOErr  string
	Resp   *C2BResp
}

func (w *C2BWrapper) Execute() {
	urls, err := w.ParseUrls()
	if err != nil {
		fmt.Printf("Error:%v\n", err)
		return
	}

	threadChan := make(chan int, w.ThreadNum)
	resChan := make(chan *B2CRespWrapper, w.ThreadNum)
	urlWg := sync.WaitGroup{}

	for _, url := range urls {
		urlWg.Add(1)
		go func(url *UrlWrapper) {
			threadChan <- 0
			defer func() {
				urlWg.Done()
				<-threadChan
			}()
			resp, err := Post(url, w.LocalFileAddr, w.DstDir)
			respWrapper := B2CRespWrapper{}
			respWrapper.SrcUrl = url.TargetUrl
			if err != nil {
				respWrapper.IOErr = err.Error()
			} else {
				respWrapper.IOErr = ""
			}
			if resp == nil {
				respWrapper.Resp = &C2BResp{}
			} else {
				respWrapper.Resp = resp
			}
			resChan <- &respWrapper
		}(url)
	}
	go func() {
		urlWg.Wait()
		close(resChan)
	}()
	for r := range resChan {
		fmt.Printf("SrcUrl:%v\n", r.SrcUrl)
		fmt.Printf("IOError:%v\n", r.IOErr)
		fmt.Printf("Code:%v\n", r.Resp.Code)
		fmt.Printf("Msg:%v\n", r.Resp.Msg)
	}
}

func Post(urlWrapper *UrlWrapper, fileAddr string, dstDir string) (resp *C2BResp, err error) {
	file, err := os.Open(fileAddr)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("uploadFile", filepath.Base(fileAddr))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)
	writer.WriteField("fileName", filepath.Base(fileAddr))
	writer.WriteField("dir", dstDir)

	err = writer.Close()
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	if len(urlWrapper.ProxyUrl) != 0 {
		proxyUrl, _ := url.Parse(urlWrapper.ProxyUrl)
		transport := &http.Transport{}
		transport.Proxy = http.ProxyURL(proxyUrl)
		client.Transport = transport
	}

	req, err := http.NewRequest("POST", urlWrapper.TargetUrl, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	postResp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer req.Body.Close()

	respBody, _ := ioutil.ReadAll(postResp.Body)
	r := &C2BResp{}
	json.Unmarshal([]byte(respBody), r)
	return r, nil
}
